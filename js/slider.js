$( document ).ready(function() {
  $('#petslide, #pethomeslide, #petslideM, #pethomeslideM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#pet, #petM').show();
  });
  $('#wfhslide, #wfhomeslide, #wfhslideM, #wfhomeslideM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#wfh, #wfhM').show();
  });
  $('#fitnesslide, #fitnesslidepre, #fitnesslideM, #fitnesslidepreM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#fitness, #fitnessM').show();
  });
  $('#mentalslide, #mentalslide, #mentalslideM, #mentalslideM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#mental, #mentalM').show();
  });
  $('#careerslide, #carrerprevslide, #careerslideM, #carrerprevslideM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#career, #careerM').show();
  });
  $('#bookslide, #writebookprev, #bookslideM, #writebookprevM').on('click', function(){
    $(this).parents().find('.slide-main-container').hide();
    $('#book, #bookM').show();
  });
});